package onc.mmr.datastructures.array;

import onc.mmr.utils.ArrayPrint;

public class ArrayLeftRotation {

    public static void main(String[] args){
        int[] array = { 1, 2, 3, 4, 5, 6, 7 };
        leftRotation(array, 2, 7);
        ArrayPrint.printArray(array, array.length);
    }

    static void leftRotation(int arr[], int d, int n){
        for (int i = 0; i < d; i++)
            leftRotateByOne(arr, n);
    }
    static void leftRotateByOne(int arr[], int n)
    {
        int i, temp;
        temp = arr[0];
        for (i = 0; i < n - 1; i++){
            arr[i] = arr[i + 1];}
        arr[i] = temp;
    }


}
