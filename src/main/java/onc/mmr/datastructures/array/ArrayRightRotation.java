package onc.mmr.datastructures.array;

import onc.mmr.utils.ArrayPrint;

public class ArrayRightRotation {

    public static void main(String[] args){
        int[] array = { 1, 2, 3, 4, 5, 6, 7 };
        System.out.println("Before Rotation");
        ArrayPrint.printArray(array, array.length);

        rightRotation(array, 2, 7);

        System.out.println("After Rotation");
        ArrayPrint.printArray(array, array.length);
    }

    static void rightRotation(int[] arr, int d, int n){
        for(int i=0; i < d; i++)
            rightRotationOne(arr, n);
    }

    static void rightRotationOne(int[] arr, int n){

        int i, temp;
        temp = arr[n-1];

        for(i = n-1; i > 0; i--){
            arr[i] = arr[i-1];
        }
        arr[i] = temp;
    }
}
